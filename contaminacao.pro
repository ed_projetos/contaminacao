TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += \
    matriz/stateview.cpp \
    aluno/main.cpp \
    game/game_engine.cpp \
    game/game_logics.cpp \
    game/game_renderer.cpp \
    game/game_input_processor.cpp

HEADERS += \
    matriz/matriz.h \
    matriz/stateview.h \
    matrizview.h \
    game/game_engine.h \
    game/game_logics.h \
    game/game_renderer.h \
    game/game_input_processor.h

LIBS += -lpthread -lsfml-graphics -lsfml-window -lsfml-system

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

OTHER_FILES += input/cenario-1.in

CONFIG(release, debug|release) {
    DESTDIR = release
} else {
    DESTDIR = debug
}

copyfiles.commands += @echo "NOW COPYING ADDITIONAL FILES" &
copyfiles.commands += cp -r $${PWD}/input $${DESTDIR}/ &
copyfiles.commands += cp -r $${PWD}/resources $${DESTDIR}/

QMAKE_EXTRA_TARGETS += copyfiles
POST_TARGETDEPS += copyfiles
