#include "game/game_engine.h"

#include <iostream>

int main() {
    GameLogics logics(10, 10, "wk");
    GameEngine game(logics);

    game.start();

    return EXIT_SUCCESS;
}
