#ifndef GAMELOGICS_H
#define GAMELOGICS_H

#include "matriz/matriz.h"

#include <SFML/Graphics.hpp>

class GameEngine;

class GameLogics
{
public:
    GameLogics(unsigned int nl, unsigned int nc, std::string const &dontuse = "");
    GameLogics(string board_file);

    void processMovement(GameEngine &engine, sf::Vector2u const &cell);
    bool hasFinished();

    inline long int getPoints() { return points; }

    inline matrix<char> const &getCurrentBoard() const { return board; }
protected:
    long int points;
    matrix<char> board;

};

#endif // GAMEENGINE_H
