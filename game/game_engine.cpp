#include "game_engine.h"

#include <SFML/Window.hpp>

GameEngine::GameEngine(GameLogics &logics)
    : renderer(800, 600, L"Contaminação"), input(renderer), logics(logics), game_thread(&GameEngine::gameLoop, this)
{
}

void GameEngine::start()
{
    {
        std::unique_lock<std::mutex> lock(game_mutex);

        gamestart = true;
        game.notify_one();
    }

    input.processInputs();

    game_thread.join();
}

void GameEngine::gameLoop()
{
    {
        std::unique_lock<std::mutex> lock(game_mutex);

        game.wait(lock, [&] {
            return gamestart;
        });
    }

    startTimer();

    renderer.displayBoard(logics.getCurrentBoard());

    while(!renderer.hasFinished() && !logics.hasFinished()) {
        sf::Vector2u cell;

        if((cell = input.selectCell(logics.getCurrentBoard())) != GameInputProcessor::INVALID_TARGET) {
            logics.processMovement(*this, cell);
            renderer.animateBoardSequence(movements);
            movements.clear();
        }
    }

    stopTimer();
    renderer.showGameOver(logics.getPoints(), elapsed);
}

float GameEngine::getGameTime()
{
    return timer.getElapsedTime().asSeconds();
}

void GameEngine::pushState(matrix<char> &board)
{
    movements.push_back(board);
}

void GameEngine::startTimer()
{
    timer.restart();
    elapsed = sf::seconds(0.0f);
}

void GameEngine::stopTimer()
{
    elapsed = timer.getElapsedTime();
}
