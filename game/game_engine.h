#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include "game_logics.h"
#include "game_renderer.h"
#include "game_input_processor.h"

#include "matriz/matriz.h"

#include <list>
#include <ctime>
#include <mutex>
#include <random>
#include <thread>
#include <condition_variable>

class GameEngine
{
public:
    GameEngine(GameLogics &logics);

    void start();

    void pushState(matrix<char> &board);

    float getGameTime();

private:
    bool gamestart{false};

    std::list< matrix<char> > movements;

    GameRenderer renderer;
    GameInputProcessor input;
    GameLogics &logics;

    sf::Clock timer;
    sf::Time elapsed;

    std::thread             game_thread;
    std::mutex              game_mutex;
    std::condition_variable game;

    long int points{0l};

    void gameLoop();

    void showGameOver();
    void startTimer();
    void stopTimer();
};

#endif // GAME_H
