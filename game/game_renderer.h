#ifndef GAME_RENDERER_H
#define GAME_RENDERER_H

#include "matriz/matriz.h"

#include <SFML/Graphics.hpp>

#include <list>
#include <mutex>
#include <string>
#include <thread>
#include <condition_variable>

class GameInputProcessor;
class GameEngine;

class GameRenderer
{
public:

    struct Offset {
        int left;
        int right;
        int up;
        int down;
    };

    class ColorMap
    {
    private:
        static const std::string color_list;

    public:
        //retorna uma string com os caracteres que representam cores
        static inline const std::string& getColorList() { return color_list; }

        //dado um char que representa uma cor, retorna essa cor
        //se o caractere for invalido, retorna a cor branca
        static sf::Color getColor(char color);

        //se a tecla for de cor valida, salva em cor a nova cor
        static void updateColor(const sf::Keyboard::Key key, char * cor);
    };

    GameRenderer(unsigned int width, unsigned int height, std::wstring const& title) throw (std::runtime_error);

    bool hasFinished();

    void animateBoardSequence(const std::list< matrix<char> > &states);
    void displayBoard(matrix<char> const &board);
    void showGameOver(long int points, sf::Time elapsedTime);
    void updateFromInput();

    inline void setAnimationDelay(sf::Time delay) { animation_delay = delay; }

    inline Offset getRenderMargins() { return off; }

    float getBoardSide(unsigned int linhas, unsigned int colunas);


    static char getRandomColor();
    static char getRandomColorExcept(string forbidden);

    friend class GameInputProcessor;

private:
    Offset off;

    bool animating, gameover, needs_update{false};
    matrix<char> last_state;

    static std::default_random_engine rng;
    static std::uniform_int_distribution<unsigned int> random_color;
    static std::vector<std::string::value_type> allowed; // para evitar uma realocacao a cada chamada da funcao (...)Except

    std::list< matrix<char> > states_queue;
    std::shared_ptr<sf::RenderWindow> window;

    std::mutex render_mutex, update_mutex;

    std::condition_variable input_update;

    sf::Text gameover_text;
    sf::Font fonts_cantarell;

    sf::Time animation_delay;

    // Tempo de jogo (salvo para o caso de precisar redesenhar janela
    sf::Time game_time;

    // Pontos do jogador (salvo para o caso de precisar redesenhar janela
    long int game_points;

    void renderWindow();

    void drawBoard(matrix<char> &board);
};

#endif // GAME_RENDERER_H
