#include "game_renderer.h"

#include <sstream>
#include <iostream>
#include <stdexcept>

#include <algorithm>

GameRenderer::GameRenderer(unsigned int width, unsigned int height, const wstring &title) throw (std::runtime_error) :
    window(new sf::RenderWindow(sf::VideoMode(width, height), title))
{
    animating = gameover = false;

    if(!fonts_cantarell.loadFromFile("resources/Cantarell-Regular.otf"))
        throw std::runtime_error("Arquivo de fonte nao encontrado!");

    animation_delay = sf::milliseconds(100);

    game_time = sf::seconds(0.0f);
    game_points = 0L;
    gameover_text.setCharacterSize(25);
    gameover_text.setStyle(sf::Text::Style::Bold);
    gameover_text.setColor(sf::Color::White);
    gameover_text.setFont(fonts_cantarell);

    off.up = off.left = off.right = off.down = 30;

    {
        std::unique_lock<std::mutex> lock(render_mutex);

        window->setFramerateLimit(60);
        window->setKeyRepeatEnabled(false);
        window->setVisible(false);
        window->setActive(false);
    }

}

bool GameRenderer::hasFinished()
{
    return !window->isOpen();
}

void GameRenderer::animateBoardSequence(std::list<matrix<char> > const &states)
{
    if(states.size() > 0) {
        {
            std::unique_lock<std::mutex> lock(render_mutex);

            if(states.size() > 1) {
                for(auto &state : states) {
                    states_queue.push_back(state);
                }

                animating = true;
            }
        }

        renderWindow();

        states_queue.clear();
    }
}

void GameRenderer::displayBoard(const matrix<char> &board)
{
    {
        std::unique_lock<std::mutex> lock(render_mutex);

        last_state = board;
    }

    renderWindow();
}

void GameRenderer::showGameOver(long points, sf::Time elapsedTime)
{

    game_points = points;
    game_time = elapsedTime;

    gameover = true;

    while(window->isOpen()){
        std::unique_lock<std::mutex> lock(update_mutex);

        renderWindow();

        input_update.wait(lock, [&] {
            if(needs_update)
                return !(needs_update = false);

            return false;
        });
    }
}

void GameRenderer::updateFromInput()
{
    std::unique_lock<std::mutex> lock(update_mutex);

    needs_update = true;
    input_update.notify_all();
}

void GameRenderer::renderWindow()
{
    if(!window->isOpen())
        return;

    window->setVisible(true);

    if(animating) {
        // Aqui, sinalizamos que o jogador fez um movimento e precisamos mostrar
        // as consequencias atraves de animacao.
        animating = false;

        for (auto &state : states_queue) {
            sf::Clock anim_timer;

            anim_timer.restart();
            render_mutex.lock();

            window->clear();
            last_state = state;
            drawBoard(state);
            window->display();

            render_mutex.unlock();

            if(animation_delay > anim_timer.getElapsedTime())
                sf::sleep(animation_delay - anim_timer.getElapsedTime());
        }
    } else {
        // Aqui so precisamos redesenhar o ultimo estado conhecido do tabuleiro.
        std::unique_lock<std::mutex> lock(render_mutex);

        window->clear();
        if(gameover) {
            // Apenas redesenha a tela de fim de jogo
            std::stringstream stream;

            stream << "Fim de Jogo!\n";
            stream << "Voce fez " << game_points << " pontos.\n";
            stream << "Tempo total: " << (unsigned long)game_time.asSeconds() << "s";

            gameover_text.setString(stream.str());

            sf::FloatRect text_bounds = gameover_text.getLocalBounds();
            sf::Vector2u window_size = window->getSize();

            gameover_text.setPosition((window_size.x - text_bounds.width)/2.0f, (window_size.y - text_bounds.height)/2.0f);

            window->draw(gameover_text);
        } else {
            drawBoard(last_state);
        }
        window->display();
    }
}

void GameRenderer::drawBoard(matrix<char> &board)
{
    sf::RectangleShape rect;

    register unsigned int i, j;

    unsigned int rows = board.sizeY();
    unsigned int columns = board.sizeX();
    unsigned int margin = 1;
    float side = getBoardSide(rows, columns);

    rect.setSize(sf::Vector2f(side - margin, side - margin));

    //print squares
    for(i = 0; i < rows; ++i){
        for(j = 0; j < columns; ++j){
            rect.setFillColor(ColorMap::getColor(board.get(i,j)));
            rect.setPosition(margin + off.left + j * side, margin + off.up + i * side);

            window->draw(rect);
        }
    }
}

float GameRenderer::getBoardSide(unsigned int rows, unsigned int columns) {
    sf::Vector2u dim = window->getSize();

    float screen_ratio = dim.x/(float)dim.y;
    float board_ratio = columns/(float)rows;
    float side;

    if(screen_ratio > board_ratio)
        side = (dim.y - off.up - off.down)/(float)rows;
    else
        side = (dim.x - off.left - off.right)/(float)columns;

    return side;
}


const std::string GameRenderer::ColorMap::color_list("rgbcmykw");

sf::Color GameRenderer::ColorMap::getColor(char color){
    if(color == 'k')
        return sf::Color::Black;
    else if(color == 'w')
        return sf::Color::White;
    else if(color == 'r')
        return sf::Color::Red;
    else if(color == 'g')
        return sf::Color::Green;
    else if(color == 'b')
        return sf::Color::Blue;
    else if(color == 'y')
        return sf::Color::Yellow;
    else if(color == 'm')
        return sf::Color::Magenta;
    else if(color == 'c')
        return sf::Color::Cyan;
    return sf::Color::White;
}

void GameRenderer::ColorMap::updateColor(const sf::Keyboard::Key key, char *cor){
    if (key == sf::Keyboard::K)
        *cor = 'k';
    else if(key == sf::Keyboard::W)
        *cor = 'w';
    else if(key == sf::Keyboard::R)
        *cor = 'r';
    else if(key == sf::Keyboard::G)
        *cor = 'g';
    else if(key == sf::Keyboard::B)
        *cor = 'b';
    else if(key == sf::Keyboard::C)
        *cor = 'c';
    else if(key == sf::Keyboard::Y)
        *cor = 'y';
    else if(key == sf::Keyboard::M)
        *cor = 'm';
}


std::vector<std::string::value_type> GameRenderer::allowed(GameRenderer::ColorMap::getColorList().length());
std::uniform_int_distribution<unsigned int> GameRenderer::random_color(0, GameRenderer::ColorMap::getColorList().length()-1);
std::default_random_engine GameRenderer::rng(time(NULL));

char GameRenderer::getRandomColor()
{
    return GameRenderer::ColorMap::getColorList().at(random_color(rng));
}

char GameRenderer::getRandomColorExcept(string forbidden)
{
    allowed.clear();

    std::remove_copy_if(ColorMap::getColorList().begin(),
                        ColorMap::getColorList().end(),
                        std::back_inserter(allowed),
                        [&] (std::string::value_type color) -> bool {
                            return forbidden.find(color) != std::string::npos;
                        });

    std::uniform_int_distribution<unsigned int> random_constr(0, allowed.size()-1);

    return allowed.at(random_constr(rng));
}


