#include "game_logics.h"
#include "game_engine.h"

#include <fstream>

GameLogics::GameLogics(unsigned int nl, unsigned int nc, const string &dontuse) :
    board(nl, nc, 'k')
{
    register unsigned int i, j;

    points = 0L;

    for(i = 0; i < nl; ++i) {
        for(j = 0; j < nc; ++j) {
            board.get(i, j) = GameRenderer::getRandomColorExcept(dontuse);
        }
    }
}

GameLogics::GameLogics(std::string board_file)
{
    std::ifstream bfile(board_file);

    if(!bfile)
        std::cerr << '[' << __FILE__ << ": " << __LINE__ << "] Falha ao abrir arquivo!" << std::endl;

    bfile >> board;

    bfile.close();
}

void GameLogics::processMovement(GameEngine &engine, const sf::Vector2u &cell)
{
    ++points;

    board.get(cell.y, cell.x) = 'w';
    engine.pushState(board);

    board.get(cell.y+1, cell.x) = 'w';
    engine.pushState(board);

    board.get(cell.y, cell.x-1) = 'w';
    engine.pushState(board);
}

bool GameLogics::hasFinished()
{
    return (points >= 5);
}
