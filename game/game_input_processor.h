#ifndef GAME_INPUT_PROCESSOR_H
#define GAME_INPUT_PROCESSOR_H

#include "matriz/matriz.h"
#include "game_renderer.h"

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <mutex>
#include <thread>
#include <memory>
#include <condition_variable>

class GameInputProcessor
{
public:
    static const sf::Vector2u INVALID_TARGET;

    GameInputProcessor(GameRenderer &renderer);

    sf::Vector2u selectCell(const matrix<char> &board);

    void processInputs();

protected:
    GameRenderer &renderer;

    bool mouseclicked{false};

    std::shared_ptr<sf::RenderWindow> window;
    std::mutex mouseclick_mutex;
    std::condition_variable mouseclick;

    sf::Vector2i click_position;

    sf::Vector2i waitForMouseClick();

    void cleanup();

};

#endif // GAME_INPUT_PROCESSOR_H
