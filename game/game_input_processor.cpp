#include "game_input_processor.h"

#include <limits>

const sf::Vector2u GameInputProcessor::INVALID_TARGET(std::numeric_limits<unsigned int>::max(), std::numeric_limits<unsigned int>::max());

GameInputProcessor::GameInputProcessor(GameRenderer &renderer) :
     renderer(renderer), window(renderer.window)
{
}

sf::Vector2u GameInputProcessor::selectCell(matrix<char> const &board)
{
    sf::Vector2u board_cell;
    sf::Vector2i mouse_target = waitForMouseClick();
    GameRenderer::Offset off = renderer.getRenderMargins();

    float side = renderer.getBoardSide(board.sizeY(), board.sizeX());
    unsigned int y = (mouse_target.y - off.up) / side;
    unsigned int x = (mouse_target.x - off.left) / side;

    if(mouse_target.y >= off.up && y < board.sizeY()){
        if(mouse_target.x >= off.left && x < board.sizeX()){
            board_cell.x = x;
            board_cell.y = y;

            return board_cell;
        }
    }

    return INVALID_TARGET;
}

void GameInputProcessor::processInputs()
{
    sf::Event ev;

    while(window->isOpen() && window->waitEvent(ev)) {
        window->setActive(false);

        if(ev.type == sf::Event::Closed) {
            window->close();
            cleanup();
        } else if(ev.type == sf::Event::Resized) {
            // So atualiza o tamanho da Viewport caso o renderer nao esteja desenhando
            std::unique_lock<std::mutex> r_lock(renderer.render_mutex);

            window->setView(sf::View(sf::FloatRect(0, 0, ev.size.width, ev.size.height)));
            renderer.updateFromInput();
        } else if(ev.type == sf::Event::KeyPressed) {
            switch (ev.key.code) {
            case sf::Keyboard::Escape:
            case sf::Keyboard::Q:
                window->close();
                cleanup();
                break;
            default:
                break;
            }
        } else if(ev.type == sf::Event::MouseButtonReleased && ev.mouseButton.button == sf::Mouse::Left) {
            std::unique_lock<std::mutex> m_lock(mouseclick_mutex);

            click_position = {ev.mouseButton.x, ev.mouseButton.y};

            // Mesmo com varias threads, imagino que seria importante a todas as que chamam
            // waitForMouseClick saberem que o usuario clicou.
            // (...) Nao funcionaria, pois as threads teriam que trocar o valor de *mouseclicked*,
            // e assim a primeira que trocasse bloquearia as demais por conta da condicao. Mas
            // essa possibilidade nao acontece neste contexto.
            mouseclicked = true;
            mouseclick.notify_one();
        }
    }
}

sf::Vector2i GameInputProcessor::waitForMouseClick() {
    std::unique_lock<std::mutex> lock(mouseclick_mutex);

    click_position = {-1, -1};

    mouseclick.wait(lock, [&]{
        if(mouseclicked)
            return !(mouseclicked = false);

        return false;
    });

    return click_position;
}

void GameInputProcessor::cleanup()
{
    std::unique_lock<std::mutex> lock(mouseclick_mutex);

    mouseclicked = true;
    mouseclick.notify_all();
    renderer.updateFromInput();
}
