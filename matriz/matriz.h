#ifndef MATRIZ_H
#define MATRIZ_H
#include <vector>
#include <iostream>
using namespace std;

template <class T>
class matrix{
private:
    unsigned int nl, nc;
    vector<T> data;
public:
    matrix():
        nl(0), nc(0), data() {}

    //copy constructor
    matrix(const matrix& mat):
        nl(mat.nl), nc(mat.nc), data(mat.data){}

    //Assignment operator
    matrix& operator=(const matrix<T>& other) // copy assignment
    {
        if (this != &other) {
            this->nc = other.nc;
            this->nl = other.nl;
            this->data = other.data;
        }
        return *this;
    }

    matrix(unsigned int _nl, unsigned int _nc, T init):
        nl(_nl), nc(_nc), data(nl * nc, init){}

    matrix(unsigned int _nl, unsigned int _nc, vector<T> v):
        nl(_nl), nc(_nc), data(v)
    {
        if(data.size() != nl * nc)
            cerr << "Erro na inicializacao da matriz"
                 << "Vector de tamanho invalido" << endl;
    }

    T& get(unsigned int y, unsigned int x) {
        if(y >= nl or x >= nc)
            cerr << "Posicao x = " << x << ", y = " << y << " invalida!" << endl;
        return data[x + y * nc];
    }
    T get(unsigned int y, unsigned int x) const {
        if(y >= nl or x >= nc)
            cerr << "Posicao x = " << x << ", y = " << y << " invalida!" << endl;
        return data[x + y * nc];
    }

    unsigned int sizeY() const{return nl;}

    unsigned int sizeX() const{return nc;}

    friend ostream &operator<<( ostream &output, const matrix &m ){
        output << m.sizeY() << " " << m.sizeX() << endl;
        for(unsigned int y = 0; y < m.sizeY(); y++){
            for(unsigned int x = 0; x < m.sizeX(); x++){
                output << m.get(y, x) << " ";
            }
            output << endl;
        }
        return output;
    }

    friend istream &operator>>( istream  &input, matrix &m ){
        // TODO: Ha um bug potencial: quando nl*nc eh maior que o suportado pelo tipo.
        //       Seria interessante proibir insercao de matrizes com mais elementos que UINT_MAX, por exemplo.

        input >> m.nl >> m.nc;
        m.data.clear();
        m.data.reserve(m.nl * m.nc);
        T tmp;
        for(unsigned int i = 0; i < (m.nl * m.nc); i++){
            if(input >> tmp)
                m.data.push_back(tmp);
            else{
                cerr << "erro no carregamento da matriz" << endl;
                exit(EXIT_FAILURE);
            }
        }
        return input;
    }
};

#endif // MATRIZ_H
